import { useQuasar } from 'quasar';
import { defineComponent, ref, watch } from 'vue';
import { getTaskById } from '../composables/Tasks';

export default defineComponent({
  name: 'TodoItem',
  props: {
    taskId: {
      type: Number,
      required: true
    },
    todoId: {
      type: Number,
      required: true
    },
    done: {
      type: Boolean,
      required: true
    },
    name: {
      type: String,
      required: true
    },
    time: {
      type: String,
      required: true
    }
  },
  setup(props) {
    const $q = useQuasar();
    const check = ref(props.done);

    watch(check, (newValue) => {
      const task = getTaskById(props.taskId);

      if (newValue === true) {
        const todo = task.inProcessTodos.find(
          (inProcessTodo) => inProcessTodo.id === props.todoId
        );
        todo.done = true;
        task.inProcessTodos = task.inProcessTodos.filter(
          (inProcessTodo) => inProcessTodo.id !== todo.id
        );
        task.doneTodos.push(todo);

        $q.notify({
          html: true,
          color: 'yellow-2',
          textColor: 'green',
          message:
            '<strong>Successfully Completed Task!!</strong><br />To-do List has been Added to Done section successfully',
          position: 'bottom-right',
          actions: [
            {
              icon: 'close',
              color: 'green',
              round: true,
              size: 'sm'
            }
          ]
        });

        return;
      }
      const todo = task.doneTodos.find(
        (doneTodos) => doneTodos.id === props.todoId
      );
      todo.done = false;
      task.doneTodos = task.doneTodos.filter(
        (doneTodos) => doneTodos.id !== todo.id
      );
      task.inProcessTodos.push(todo);

      $q.notify({
        html: true,
        color: 'yellow-2',
        textColor: 'green',
        message:
          '<strong>Successfully Added Task!!</strong><br />To-do List has been Added to In progress section successfully',
        position: 'bottom-right',
        actions: [
          {
            icon: 'close',
            color: 'green',
            round: true,
            size: 'sm'
          }
        ]
      });
    });

    return {
      check
    };
  }
});
