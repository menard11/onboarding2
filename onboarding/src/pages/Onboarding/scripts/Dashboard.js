import { defineComponent, ref } from 'vue';
import BoardList from '../components/BoardList.vue';

export default defineComponent({
  name: 'Dashboard',
  components: {
    BoardList
  },
  setup() {
    const search = ref('');
    return {
      search
    };
  }
});
