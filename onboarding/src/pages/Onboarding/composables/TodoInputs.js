import { ref } from 'vue';

let TodoInputs = ref([{ id: 1, taskName: null, time: null, showTime: false }]);

const addNewTodoInputs = (taskName = null, time = null) => {
  TodoInputs.value.push({
    id: TodoInputs.value.length + 1,
    taskName,
    time,
    showTime: false
  });
};

const removeTodoInputsById = (id) => {
  if (id > TodoInputs.value.length) return;

  TodoInputs.value = TodoInputs.value.filter(
    (taskInput) => taskInput.id !== id
  );
};

const clearTodoInputs = () => {
  TodoInputs = ref([{ id: 1, taskName: null, time: null, showTime: false }]);
};

const setTodoInputs = (todoInputs) => {
  TodoInputs.value = todoInputs;
};

export {
  TodoInputs,
  addNewTodoInputs,
  removeTodoInputsById,
  clearTodoInputs,
  setTodoInputs
};
